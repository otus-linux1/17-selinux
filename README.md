# Selinux

## Запустить nginx на нестандартном порту 3-мя разными способами:

Ставим в конфиге nginx порт 1080, перезапускаем, получаем статус службы

```
Oct 11 13:48:48 otus systemd[1]: Failed to start The nginx HTTP and reverse proxy server.
```

и в audit.log

```
type=AVC msg=audit(1602424128.126:1268): avc:  denied  { name_bind } for  pid=26111 comm="nginx" src=1080 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:unreserved_port_t:s0 tclass=tcp_socket permissive=0
```

### Переключатели setsebool

```
setsebool -P nis_enabled 1
# проверяем что nginx завелся, возвращаем как было
setsebool -P nis_enabled 0
```

### Добавление нестандартного порта в имеющийся тип;

кажется самый вменяемый вариант

```
semanage port -l | grep http
semanage port -a -t http_port_t -p tcp 1080
# проверяем что сервер ожил и удаляем
semanage port -d -t http_port_t -p tcp 1080
```

### Формирование и установка модуля SELinux.

```
cat /var/log/audit/audit.log | audit2allow -M mynginx
semodule -i mynginx.pp
# проверяем работоспособность и удаляем модуль
semodule -r mynginx
```

## Обеспечить работоспособность приложения при включенном Selinux

"Инженер настроил следующую схему" (с)

Инженер настроил, а я чини, охренеть вообще.

### Как чиним

Поднимаем стенд, открываем консольки сервера и клиента.

Делаем `> /var/log/audit/audit.log` на обеих машинах, чтобы там было меньше шума.

#### На клиенте

```
[vagrant@client ~]$ nsupdate -k /etc/named.zonetransfer.key
> server 192.168.50.10
> zone ddns.lab
> update add www.ddns.lab. 60 A 192.168.50.15
> send
update failed: SERVFAIL
>
```

```
cat /var/log/audit/audit.log | audit2why
```

нам говорят, что нет никаких проблем, пока забываем про клиент.

#### На сервере

```
cat /var/log/audit/audit.log | audit2allow


#============= named_t ==============

#!!!! WARNING: 'etc_t' is a base type.
allow named_t etc_t:file create;
```

Выглядит этот совет как-то не очень, поэтому идем и смотрим в аудит глазами,
ничего там не понимаем кроме того что не создался какой-то файл.

Поэтому идем в `/var/log/messages`, там на человеческом языке читаем:

```
Oct 11 15:01:06 localhost named[5440]: /etc/named/dynamic/named.ddns.lab.view1.jnl: create: permission denied
Oct 11 15:01:06 localhost named[5440]: client @0x7f201803c3e0 192.168.50.15#18064/key zonetransfer.key: view view1: updating zone 'ddns.lab/IN': error: journal open failed: unexpected error
Oct 11 15:01:09 localhost dbus[337]: [system] Activating service name='org.fedoraproject.Setroubleshootd' (using servicehelper)
Oct 11 15:01:09 localhost dbus[337]: [system] Successfully activated service 'org.fedoraproject.Setroubleshootd'
Oct 11 15:01:10 localhost setroubleshoot: SELinux is preventing isc-worker0000 from create access on the file named.ddns.lab.view1.jnl. For complete SELinux messages run: sealert -l 4ebf50e3-c6ce-4a44-acd4-2336f5ab9d77
Oct 11 15:01:10 localhost python: SELinux is preventing isc-worker0000 from create access on the file named.ddns.lab.view1.jnl.#012#012*****  Plugin catchall_labels (83.8 confidence) suggests   *******************#012#012If you want to allow isc-worker0000 to have create access on the named.ddns.lab.view1.jnl file#012Then you need to change the label on named.ddns.lab.view1.jnl#012Do#012# semanage fcontext -a -t FILE_TYPE 'named.ddns.lab.view1.jnl'#012where FILE_TYPE is one of the following: dnssec_trigger_var_run_t, ipa_var_lib_t, krb5_host_rcache_t, krb5_keytab_t, named_cache_t, named_log_t, named_tmp_t, named_var_run_t, named_zone_t.#012Then execute:#012restorecon -v 'named.ddns.lab.view1.jnl'#012#012#012*****  Plugin catchall (17.1 confidence) suggests   **************************#012#012If you believe that isc-worker0000 should be allowed create access on the named.ddns.lab.view1.jnl file by default.#012Then you should report this as a bug.#012You can generate a local policy module to allow this access.#012Do#012allow this access for now by executing:#012# ausearch -c 'isc-worker0000' --raw | audit2allow -M my-iscworker0000#012# semodule -i my-iscworker0000.pp#012
```

Идем в гугл со словами `ddns` и `selinux` и сразу же натыкаемся на
[страницу](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-managing_confined_services-bind-configuration_examples),
на которой написано, что задуманный создателем тип для `/var/named/dynamic/` это `named_cache_t`,
смотрим что у нас:

```
[root@ns01 home]# ll -alZ /etc/named/dynamic/
drw-rwx---. root  named unconfined_u:object_r:etc_t:s0   .
drw-rwx---. root  named system_u:object_r:etc_t:s0       ..
-rw-rw----. named named system_u:object_r:etc_t:s0       named.ddns.lab
-rw-rw----. named named system_u:object_r:etc_t:s0       named.ddns.lab.view1
```

В этот момент становится понятно, что имел ввиду `audit2allow`. 
Выполняем команду, приведенную по вышеуказанной ссылке:

```
restorecon -R -v /var/named/dynamic
```

и понимаем что ничего не изменилось.

Еще немного гуглим, находим еще одну [мурзилку](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-security-enhanced_linux-working_with_selinux-selinux_contexts_labeling_files),
и меняем тип как написано в ней:

```
semanage fcontext -a -t named_cache_t "/etc/named/dynamic(/.*)?"
restorecon -R -v /etc/named/dynamic
```

получаем в ответ:

```
restorecon reset /etc/named/dynamic context unconfined_u:object_r:etc_t:s0->unconfined_u:object_r:named_cache_t:s0
restorecon reset /etc/named/dynamic/named.ddns.lab context system_u:object_r:etc_t:s0->system_u:object_r:named_cache_t:s0
restorecon reset /etc/named/dynamic/named.ddns.lab.view1 context system_u:object_r:etc_t:s0->system_u:object_r:named_cache_t:s0
```

#### И снова на клиенте 

Возвращаемся в консоль клиента, повторяем там процедуру добавления A записи,
в этот раз она проходит без ошибок.

После этого делаем

```
dig @192.168.50.10 www.ddns.lab
```

получаем

```
; <<>> DiG 9.11.4-P2-RedHat-9.11.4-16.P2.el7_8.6 <<>> @192.168.50.10 www.ddns.lab
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45885
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.ddns.lab.                  IN      A

;; ANSWER SECTION:
www.ddns.lab.           60      IN      A       192.168.50.15

;; AUTHORITY SECTION:
ddns.lab.               3600    IN      NS      ns01.dns.lab.

;; ADDITIONAL SECTION:
ns01.dns.lab.           3600    IN      A       192.168.50.10

;; Query time: 0 msec
;; SERVER: 192.168.50.10#53(192.168.50.10)
;; WHEN: Sun Oct 11 16:24:29 UTC 2020
;; MSG SIZE  rcvd: 96
```

## Итог


Все получилось, мы восхитительны.

PS.

Добавил в плейбук шаг

```
- name: set selinux type for /etc/named/dynamic and content
  file:
    path: /etc/named/dynamic
    setype: named_cache_t
    recurse: yes
```

теперь стенд работает сразу после `vagrant up`


## Update. "Программист должен быть внимательным" (с)

Цитата:

> Идем в гугл со словами `ddns` и `selinux` и сразу же натыкаемся на
[страницу](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-managing_confined_services-bind-configuration_examples),
на которой написано, что задуманный создателем тип для `/var/named/dynamic/` это `named_cache_t`,

Вот тут я должен был заметить что там `/var`, а у меня `/etc`, но не заметил :facepalm:

Таинственный инженер сложил файлы в `/etc/named` вместо `/var/named`,
поэтому не надо трогать типы selinux, надо исправить плейбук и конфиг
и все будет работать без дополнительных манипуляций.
